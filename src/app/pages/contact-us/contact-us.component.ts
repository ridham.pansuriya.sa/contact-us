import { validateVerticalPosition } from '@angular/cdk/overlay';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, UntypedFormArray,  UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { ReCaptcha2Component, ReCaptchaV3Service } from 'ngx-captcha';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  @ViewChild('captchaElem') captchaElem!: ReCaptcha2Component;

  allFiles: File[] = [];
  registerForm: FormGroup;
  MainArray:any;
  valueforedit:any;
  failfile:File[]=[];
  editForm=false;
  isSubmited=false;
  siteKey:any='6LeH9gkjAAAAALBHlk_pZe50tNIIJF_WSJsAM1N_';
  fileerrorMsg:any;
  fileupload=true;
  captcha=true;
  errorname='';
  emailRegEx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';
  mobileRegex='^[0-9]{10}$';
  dynamicFieldArray=[
    {
      name:'hooby',
      validator:[Validators.required ],
      type:'text'
    },
    {
      name:'descriptiopn',
      validator:[Validators.required],
      type:'textarea'
    },
    {
      name:'email',
      validator:[Validators.required,Validators.email],
      type:'textarea'
    },
    {
      name:'phone 0',
      validator:[Validators.required,Validators.pattern(this.mobileRegex)],
      type:'number'
    },
    {
      name:'fav Actress',
      item1:'indian',
      item2:'turkish',
      validator:[Validators.required],
      type:'radio'
    },
  ]
  constructor(   private reCaptchaV3Service: ReCaptchaV3Service    ){

  }
  ngOnInit(): void {
    const emailRegEx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';
    this.MainArray=[]
    this.registerForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      recaptcha:new FormControl('',[])
  });
  
   for(let i=0;i<this.dynamicFieldArray.length;i++){
    // this.registerForm.push({
    //   this.dynamicFieldArray[i].name:new FormControl('',[this.dynamicFieldArray[i].validator])
    // })
    this.registerForm.addControl(this.dynamicFieldArray[i].name, new FormControl('',this.dynamicFieldArray[i].validator));
    console.log(this.registerForm);
   
   
  }
  if(this.captcha==true){
    this.registerForm.controls["recaptcha"].setValidators([Validators.required]);
  }
  
  }

  handleReset(){
  }
  handleExpire(){
  }
  handleLoad(){
  }
  handleSuccess(item:any){
    console.log(item);
  }
  
  onSubmit(){
    this.isSubmited=true;
  console.log('dd',this.registerForm);
  
    if(this.registerForm.valid){
      this.isSubmited=false;
      let value =this.registerForm.value;
      this.MainArray.push(value);
      console.log(this.MainArray);
      console.log(this.allFiles);
    
      this.registerForm.reset();
      this.removeFile();
    }
    
  }
  droppedFiles(allFiles: File[]): void {
    this.failfile=[];
    this.allFiles=[];
    const filesAmount = allFiles.length;    
    
    for (let i = 0; i < filesAmount; i++) {
      
      if(allFiles[i].type.includes('image') || allFiles[i].type == 'application/pdf' ){
        if(allFiles[i].size<1000000){
          
          const file = allFiles[i];
          this.allFiles.push(file);
        }
        else{
          this.fileerrorMsg='file size is more than 1 mb';
          console.error();
          this.failfile.push(allFiles[i]);
        }
      }
      
      else{
        this.fileerrorMsg='file type should png , jpeg , jpg , pdf '
        this.failfile.push(allFiles[i]);
      }
      
    }
  }
  selectedfile(event:any){
    const file=event.target.files;   
    this.droppedFiles(file);
  }
  removeFile(){
    this.allFiles=[];
    this.failfile=[];
  }


}
