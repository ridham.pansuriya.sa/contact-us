import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxCaptchaModule } from 'ngx-captcha';
import { AppComponent } from './app.component';

const routes: Routes = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },  
    {
    path: 'main',
    component: AppComponent,
    children:[
    
      {
        path: '',
        loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule),
      },
     
      
    ]
  },
];

@NgModule({

  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }),NgxCaptchaModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
