# CONTACT-US UI

This project is about CONTACT-US UI.
You can customize file upload and captcha and file upload validations

## Project Prerequisite

1. Angular CLI v13.0.0
2. Node.js v16.16.0
3. Project uses [ngx-bootstrap](https://www.npmjs.com/package/ngx-bootstrap) v8.0.0, To add ngx-bootstrap : <br /> `ng add ngx-bootstrap@8.0.0`

## Running Development server

1. `npm i` in root directory
2. `ng serve` for dev server (`http://localhost:4200/`)



## Adding Component to your project

- Copy contact-us component [folder](path) to your project


```

@NgModule({
  ...
  imports: [
    ...
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatRadioModule,
    NgxCaptchaModule
    ...
  ],
  ...
})
```

- Implementation - 1 : Show using routing

```
// in module routing  file:
...
    {
        path: 'contact-us', component: ContactUsComponent,
    },
...

// in html  file:
...
 <li class="nav-item active">
          <a class="nav-link" routerLink="contact-us" >contact us </a>
 </li>
...

...
you will find the data in console and use it by calling api in ts file
...
```



## Documentation

See [appComponent](path) for example.


&nbsp;&nbsp;Display contact-us form compoent to UI with given params many are configurable.
| Params | Type | Default | Description |
| ----------- | ------ | ------------------------------------------- | ------------------------------------------------------------------------- |
| allFiles | array | []| successfully uploaded file will be stored in it. |
| failfile | array | [] | invalid file for upload will be stored in it. |
| isSubmited | boolean | default | for validation |
| siteKey | string   | '   ' | you have to generate and put it for recaptcha verification|
| fileerrorMsg | string | '   ' | forfile validation error  string value store |
| fileupload | boolean | true | flag for enable and disable file upload |
| captcha | boolean | true | flag for enable and disable recaptcha verification|
| dynamicFieldArray | [] | array of object(you can see in this example ) | by configure this variable you can configura whole form you have to just add name of field validators and input type of that field |


